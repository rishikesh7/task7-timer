import React from 'react';
import './timer.css';

interface Props {}

interface State {
	days: number,
	hours: number,
	minutes: number,
	seconds: number,
	time: number,
	timerId: number
}

class Timer extends React.Component<Props,State> {

	constructor(props: Props){
		super(props)

		this.state = {
			time: 172801,
			days: 0,
			hours: 0,
			minutes: 0,
			seconds: 0,
			timerId: 0,
		}
	}

	timerUpdate = () =>{

		const { time } = this.state

		if(time === 0)
			clearInterval(this.state.timerId);

		let days        = Math.floor(time/24/60/60);
		let hoursLeft   = Math.floor((time) - (days*86400));
		let hours       = Math.floor(hoursLeft/3600);
		let minutesLeft = Math.floor((hoursLeft) - (hours*3600));
		let minutes     = Math.floor(minutesLeft/60);
		let remainingSeconds = time % 60;

		this.setState({
			days,
			hours,
			minutes,
			seconds: remainingSeconds,
			time: time - 1
		})
	}

	resetTimer = () => {
		this.setState({time: 172801})
	}

	componentDidMount = () => {
		const timerId : number = window.setInterval(this.timerUpdate,1000);
		this.setState({timerId})
	}

	componentWillUnmount = () => {
		clearInterval(this.state.timerId)
	}

	render(){

		const { days, hours, minutes, seconds } = this.state;

		return (
			<div className="timer">
				<div className="timer-heading"> Timer </div>
				<div className="timer-time">
					{days ? (<div className="timer-time-quantity">{days + " days"}</div>) : null}
					{hours ? (<div className="timer-time-quantity">{hours + " hours"}</div>) : null}
					{minutes ? (<div className="timer-time-quantity">{minutes + " minutes"}</div>) : null}
					{seconds ? (<div className="timer-time-quantity">{seconds + " seconds"}</div>) : null}
				</div>
				<div className="timer-buttons">
					<div className="timer-button" onClick={() => {this.resetTimer()}}> Reset </div>
				</div>
			</div>
		)
	}
}

export default Timer